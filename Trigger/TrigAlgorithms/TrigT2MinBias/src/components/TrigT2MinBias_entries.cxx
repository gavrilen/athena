#include "../TrigCountSpacePointsMT.h"
#include "../SPCountHypoAlgMT.h"
#include "../SPCountHypoTool.h"
#include "../MbtsFexMT.h"
#include "../MbtsHypoAlg.h"
#include "../MbtsHypoTool.h"

DECLARE_COMPONENT( TrigCountSpacePointsMT )
DECLARE_COMPONENT( SPCountHypoAlgMT )
DECLARE_COMPONENT( SPCountHypoTool )
DECLARE_COMPONENT( MbtsFexMT )
DECLARE_COMPONENT( MbtsHypoAlg )
DECLARE_COMPONENT( MbtsHypoTool )
