#include "../TrigSignatureMoniMT.h"
#include "../DecisionCollectorTool.h"
#include "../SchedulerMonSvc.h"
#include "../TrigErrorMonTool.h"

DECLARE_COMPONENT( TrigSignatureMoniMT )
DECLARE_COMPONENT( DecisionCollectorTool )
DECLARE_COMPONENT( SchedulerMonSvc )
DECLARE_COMPONENT( TrigErrorMonTool )
